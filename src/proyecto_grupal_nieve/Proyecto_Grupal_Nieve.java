/****************************************************
 PRACTICA_1 DE M08 - Desplegament d'aplicacions web -
 UF4 - Control de Versions -
 - Tasques a realitzar en grup -
 ****************************************************/
package Proyecto_Grupal_Nieve;
/*
Autor : Nieve A. Fernández Salazar
 */
public class Proyecto_Grupal_Nieve {
  
    public static void main(String[] args) {
          System.out.println("PRACTICA_1 UF4 del M08");
          System.out.println("**********************");
     }    
    
    public static void metodo_sin_conflicto(){ //*********PRIMER METODO
            System.out.print("Código Inicial, editado por Nieve...");
            System.out.println("Nieve modifica la linea para el paso 13.1");
           //linea borrada por Eli
      }
      
    public static void metodo_con_conflicto(){//**********SEGUNDO METODO
            System.out.print(" creo conflicto ELI");
      }
}
